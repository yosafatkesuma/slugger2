@extends('layouts.app')
@section('content')
    @parent
    @if (session('message'))
        <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong> {{ session('message') }} </strong>
        </div>
    @endif
   <div style="width: 88%;margin:0 auto;">
    <form action="{{url('checko')}}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <table class="table table-striped">
            <thead>
                <tr>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Jumlah</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $row)
                    <tr>
                    <td><input type="text" class="form-control border-input" value="{{$row->Kode}}" readonly="true" name="kode[]"></td>
                    <td><input type="text" class="form-control border-input" value="{{$row->Nama}}" readonly="true" name="nama[]"></td>
                    <td><input type="number" id="harga" class="form-control border-input" value="{{$row->Harga}}" readonly="true" name="harga[]"></td>
                    <td><input type="number" class="form-control border-input" value="{{$row->Jumlah}}" min="1" name="jumlah[]" readonly="true" ></td>
                    <td><input type="number" class="form-control border-input" value="{{$row->Jumlah * $row->Harga}}" name="totalh" readonly="true"></td>
                    </tr> 
                @endforeach
                <tr>Total Harga Checkout : {{$harga}}</tr> 
            </tbody>
        </table>
        <div>
        </div>
        <div align="right" style="margin-right: 15px;">
            <button for="submit" class="btn btn-primary">Check Out</button>
        </div>
    </form>
    </div>
@endsection