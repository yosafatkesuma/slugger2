@extends('layouts.app')

@section('content')
@parent
@if (session('message'))
    <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{ session('message') }} </strong>
    </div>
@endif
<div class="container">
    @foreach ($products->chunk(5) as $productchunks)
    <div class="row">
        @foreach ($productchunks as $product)
        <form action="{{url('carti')}}" method="POST" name="add">
                {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$product->id}}">
            <input type="hidden" name="nama" value="{{$product->title}}">
            <input type="hidden" name="harga" value="{{$product->price}}">
            
            <div class="box">
                <div class="thumbnail">
                    <img src="https://b451c108ef7ce3b912eb-75c7695d67180639ae25fac6b37d4ead.ssl.cf3.rackcdn.com/onlinereality/uploads/prod_img/2_40698_e.jpg" alt="...">
                    <div>
                        <h3>{{$product->title}}</h3>
                        <p>{{$product->description}}</p>
                        <div class="clearfix">
                            <div class="price">Rp {{$product->price}}</div>
                            <input type="number" name="jumlah" value="1" min="1" style="width:50px;">
                            
                        <button class="btn" role="button" type="submit">Add to Cart</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @endforeach
    </div>    
    @endforeach
</div>
@endsection
