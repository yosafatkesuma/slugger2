<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//     return view('layouts.app');
///});
Route::get('/', 'productController@getindex');
Auth::routes();

Route::get('cart',[
    'uses'=>'productController@getAddtoCart',
    'as'=>'cart'
]);

Route::resource('carti', 'CartController');
Route::get('carti',[
    'uses'=>'CartController@index',
    'as'=>'carti'
]);
Route::post('checko','CartController@checkoutend');
Route::get('checko','CartController@checkoutend');