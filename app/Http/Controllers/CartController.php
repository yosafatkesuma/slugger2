<?php

namespace App\Http\Controllers;
use DB;
use App\cart;
use App\endcart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{ 
    public function store(Request $request)
    {
        //$cart = DB::table('cart')->get();
        $o = new cart;
        $o->Kode = $request->id;
        $o->Nama = $request->nama;
        $o->Harga = $request->harga;
        $o->Jumlah = $request->jumlah;
        $o->Jumlah_Harga = $request->harga * $request->jumlah;
        $o->created_by = Auth::user()->name;
        $o->save();
        $data = cart::all();
        //return view('cart', array('data' => $data));
        return redirect()->back()->with('message', 'Data Sudah tersimpan');
    }
    public function index(){
        $data = cart::all();
        $harga = cart::getTotalHarga();
        return view('cart', array(
            'data' => $data,
            'harga' =>$harga,
        ));
    }

    public function getTotalprice(){
        $cart = DB::table('cart')->get();
    }

    public function checkoutend(){
        $endcart = new endcart;
        $endcart->Nama = Auth::user()->name;
        $endcart->TBayar = DB::table('cart')->sum('Jumlah_Harga');
        $endcart->save();
        DB::table('cart')->delete();
        return redirect('/');
    }
}

