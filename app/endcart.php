<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class endcart extends Model
{
    protected $table = 'endcart';
    protected $foreignKey = 'Nama';
    public $incrementing = false;
    public $timestamps = false;
}
