<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    public static function getAllProduct(){
        return self::all();
    }

    protected $filltable = ['imagePath','title','description','price'];
}
